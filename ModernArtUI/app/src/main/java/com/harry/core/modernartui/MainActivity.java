package com.harry.core.modernartui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;


public class MainActivity extends Activity {

    final static int MAX_PROGRESS = 255;


    SeekBar seekBar;
    LinearLayout l1, l2, l3, l4, l5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = (SeekBar) findViewById(R.id.changecolor);
        l1 = (LinearLayout) findViewById(R.id.l1);
        l2 = (LinearLayout) findViewById(R.id.l2);
        l3 = (LinearLayout) findViewById(R.id.l3);
        l5 = (LinearLayout) findViewById(R.id.l5);

        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBar.setMax(MAX_PROGRESS);

        updateBackground(seekBar.getProgress());

    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    updateBackground(i);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            };

    private void updateBackground(int i){

        l1.setBackgroundColor(Color.argb(MAX_PROGRESS, i, 255, 128));

        l2.setBackgroundColor(Color.argb(MAX_PROGRESS, 1, 128, i));

        l3.setBackgroundColor(Color.argb(MAX_PROGRESS, 255, 1, i));

        l5.setBackgroundColor(Color.argb(MAX_PROGRESS,i, 1, 128));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            OpenPageDialog openPageDialog = new OpenPageDialog();
            openPageDialog.show(getFragmentManager(), "Dialog called");
        }
        return super.onOptionsItemSelected(item);
    }
}
