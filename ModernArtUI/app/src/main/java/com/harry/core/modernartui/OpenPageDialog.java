package com.harry.core.modernartui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;


/**
 * Created by core on 02.11.14.
 */
public class OpenPageDialog extends DialogFragment {

    private static final String TAG = "Dialog Fragment";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use builder class for dialogs
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.goto_web_dialog_title)
                .setPositiveButton(R.string.goto_web_positiv, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    //goto MOMA
                        Log.v(TAG, "go to moma");
                        goToMoma();
                    }
                })
                .setNegativeButton(R.string.goto_web_negativ, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    //cancel
                        Log.v(TAG, "Cancel");
                    }
                });
        //return object
        return builder.create();

    }

    public void goToMoma() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.moma.org/interactives/exhibitions/2007/serra/"));
        startActivity(browserIntent);
    }
}
